FROM phreekbird/cloudcli:latest
MAINTAINER phreekbird webmaster@phreekbird.net
# install ansible and unzip
RUN apt-add-repository ppa:ansible/ansible
RUN DEBIAN_FRONTEND=noninteractive apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install unzip ansible -y
# install terraform
RUN mkdir /opt/terraform
RUN wget https://releases.hashicorp.com/terraform/0.11.1/terraform_0.11.1_linux_amd64.zip
RUN unzip terraform_0.11.1_linux_amd64.zip -d /opt/terraform
RUN echo "export PATH=$PATH:~/opt/terraform/bin" >> ~/.bash_profile
